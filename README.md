# Streamtip API Client for GO

[![Build Status](https://drone.io/bitbucket.org/muxy/streamtip/status.png)](https://drone.io/bitbucket.org/muxy/streamtip/latest)
[![MIT](http://img.shields.io/badge/license-MIT-green.svg)](LICENSE)
