package streamtip

import "fmt"

type MeResponse struct {
	Status        int64         `json:"status"`
	User          User          `json:"user"`
	Authorization Authorization `json:"authorization"`
}

type User struct {
	ID string `json:"_id"`
}

type Authorization struct {
	Client  string `json:"client"`
	Expires string `json:"expires"`
}

func (client *StreamTipClient) GetMe() (*MeResponse, error) {
	var me MeResponse
	var e StreamtipError

	resp, err := client.session.Get("https://streamtip.com/api/me", nil, &me, &e)
	if err != nil {
		return nil, err
	}

	if resp.Status() != 200 {
		return nil, fmt.Errorf("Status: %d, Message: %s", resp.Status(), e.Message)
	}

	return &me, nil
}
