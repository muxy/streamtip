package streamtip

import (
	"net/http"

	"gopkg.in/jmcvetta/napping.v3"
)

type StreamTipClient struct {
	session napping.Session
}

func NewStreamTipClient(httpClient *http.Client) StreamTipClient {
	return StreamTipClient{
		session: napping.Session{
			Client: httpClient,
		},
	}
}

type StreamtipError struct {
	Status  int64  `json:"status"`
	Message string `json:"message"`
}
