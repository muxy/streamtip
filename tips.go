package streamtip

import (
	"fmt"
	"net/url"
	"strconv"
	"time"
)

type TipsResponse struct {
	Tips   []Tip `json:"tips"`
	Count  int64 `json:"_count"`
	Limit  int64 `json:"_limit"`
	Offset int64 `json:"_offset"`
	Status int64 `json:"status"`
}

type Tip struct {
	ID             string    `json:"_id"`
	Amount         float64   `json:"amount,string"`
	Cents          int64     `json:"cents"`
	Channel        string    `json:"channel"`
	CurrencyCode   string    `json:"currencyCode"`
	CurrencySymbol string    `json:"currencySymbol"`
	Date           time.Time `json:"date"`
	Email          string    `json:"email"`
	Goal           string    `json:"goal"`
	FirstName      string    `json:"firstName"`
	LastName       string    `json:"lastName"`
	Note           string    `json:"note"`
	Processor      string    `json:"processor"`
	TransactionID  string    `json:"transactionId"`
	Username       string    `json:"username"`
}

func (client *StreamTipClient) GetTips(limit, offset int64) (*TipsResponse, error) {
	var tips TipsResponse
	var e StreamtipError

	v := &url.Values{}
	v.Add("limit", strconv.FormatInt(limit, 10))
	v.Add("offset", strconv.FormatInt(offset, 10))

	resp, err := client.session.Get("https://streamtip.com/api/tips", v, &tips, &e)
	if err != nil {
		return nil, err
	}

	if resp.Status() != 200 {
		return nil, fmt.Errorf("Status: %d, Message: %s", resp.Status(), e.Message)
	}

	return &tips, nil
}
