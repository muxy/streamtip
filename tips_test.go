package streamtip_test

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"bitbucket.org/muxy/streamtip"
)

func TestDecodeExample(t *testing.T) {
	exampleData := `
{
    "_count" : 1,
    "_limit" : 1,
    "_offset" : 0,
    "status" : 200,
    "tips" : [
        {
            "_id" : "5517a758964def7137164238",
            "amount" : "100.00",
            "cents" : 10000,
            "channel" : "5456997a68db94ce04a5f4c3",
            "currencyCode" : "USD",
            "currencySymbol" : "$",
            "date" : "2014-06-04T22:58:01.202Z",
            "email" : "concerndoge@test.com",
            "user" : {
                "_id" : "5456997a68db94ce04a5f4c3",
                "avatar" : "https://link.to/avatar/image.png",
                "displayName" : "Test",
                "name" : "test",
                "provider" : "twitch",
                "providerId" : 10101
            },
            "goal" : "5455caccdd398acb7a28c3f0",
            "firstName" : "Test",
            "lastName" : "User",
            "note" : "such wow, very test",
            "processor" : "PayPal",
            "transactionId" : "4K2N0D835234BWKC",
            "username" : "im_a_test_user"
        }
    ]
}
`

	var response streamtip.TipsResponse
	err := json.Unmarshal([]byte(exampleData), &response)
	if err != nil {
		t.Error(err)
	}

	if len(response.Tips) != 1 {
		t.Error("Tips list was the wrong length")
	}

	time, err := time.Parse(time.RFC3339, "2014-06-04T22:58:01.202Z")
	if err != nil {
		t.Error("Failed to parse time")
	}

	tip := streamtip.Tip{
		ID:             "5517a758964def7137164238",
		Amount:         100.00,
		Cents:          10000,
		Channel:        "5456997a68db94ce04a5f4c3",
		CurrencyCode:   "USD",
		CurrencySymbol: "$",
		Date:           time,
		Email:          "concerndoge@test.com",
		Goal:           "5455caccdd398acb7a28c3f0",
		FirstName:      "Test",
		LastName:       "User",
		Note:           "such wow, very test",
		Processor:      "PayPal",
		TransactionID:  "4K2N0D835234BWKC",
		Username:       "im_a_test_user",
	}

	if !reflect.DeepEqual(response.Tips[0], tip) {
		t.Errorf("Donation objects not equal, expected: %v got: %v", tip, response.Tips[0])
	}
}
